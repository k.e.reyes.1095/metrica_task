package com.java.maven.metrica_task;

import org.junit.Assert;
import org.junit.Test;


/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
	
	private String INPUT = "myInput";
	
    @Test
    public void testMyFunction()
    {
    	String expected = "myInput with myFunction";
    	Assert.assertEquals(expected, App.myFunction(INPUT));
    }
}
