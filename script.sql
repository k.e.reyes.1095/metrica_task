-- Crear tabla de proveedores
CREATE TABLE proveedores (
	id_proveedor INTEGER NOT NULL,
	nombre VARCHAR(50) NOT NULL,
	fecha_alta DATE NOT NULL,
	id_cliente int NOT NULL,
	PRIMARY KEY (id_proveedor)
);

-- Insertar datos en la tabla
INSERT INTO proveedores VALUES(1, "Coca-cola", CURDATE() ,5);
INSERT INTO proveedores VALUES(2, "Pepsi", CURDATE() ,5);
INSERT INTO proveedores VALUES(3, "Redbull", CURDATE() ,6);

--SELECT * FROM proveedores;