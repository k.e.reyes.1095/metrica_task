package com.java.maven.metrica_task;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.java.maven.metrica_task.domain.Proveedor;
import com.java.maven.metrica_task.utils.ConexionMySQL;

public class App 
{
	private static final String selectQuery = "SELECT * FROM proveedores WHERE id_cliente=%d";
	private static final Logger log = LoggerFactory.getLogger(App.class);
	
	private static ConexionMySQL conSQL = new ConexionMySQL();
	
    public static void main( String[] args )
    {
    	if(args.length < 1) {
    		log.error("Proporcione una entrada.");
    		System.exit(-1);
    	}
    	
    	// Obtenemos el id del cliente
    	Integer id_cliente = null;
    	
    	try {
    		id_cliente = Integer.parseInt(args[0]);
    	} catch (NumberFormatException nfe) {
    		log.error("El primer argumento debe ser un entero.");
            System.exit(-1);
		}
    	
    	// Creamos una query personalizada para el cliente dado
    	String customQuery = String.format(selectQuery, id_cliente);
        
    	// Nos conectamos a mySQL y creamos una lista de Proveedores para almacenar los resultados
        Connection conn = null;
        List<Proveedor> proveedores = new ArrayList<>();
        
        try {
        	conn = conSQL.conectarMySQL(); //user, password
        	
        	log.info("Conexión a la base de datos efectuada correctamente");
        	
        	Statement sentence = conn.createStatement();
        	ResultSet rs = sentence.executeQuery(customQuery);
        	
        	while (rs.next()) {
        		
        		// Al conocer la estructura de la base de datos ya sabemos que campos tendra cada proveedor.
        		proveedores.add(
        				new Proveedor(
        						rs.getInt(1),
        						rs.getString(2),
        						rs.getDate(3),
        						rs.getInt(4)));

        	}
        	
        	// Creamos la ruta del output para guardar el fichero que vamos a generar
        	
        	
        	String filename = String.format("proveedores_cliente%d_%s.txt", id_cliente, UUID.randomUUID().toString());
        	Path pathOutput = Paths.get("output").toAbsolutePath();
        	Path filePath = pathOutput.resolve(filename);
        			//Paths.get("output").resolve(filename).toAbsolutePath();
        	
        	File folder = pathOutput.toFile();
        	
        	if (!folder.exists()){
        		folder.mkdirs();
        	}
        	
        	File file = filePath.toFile();
        	
        	
        	// Si no hemos podido recuperar ningún proveedor, 
        	// escribimos por el log que no hay proveedores asignados
    		if (proveedores.size() == 0) {
    			log.info("El cliente '" + id_cliente + "' no tiene proveedores asignado");
    		} else if (file.exists()) {
        		log.info("Fichero " + filename + " ya existe");
        	} else {
        		BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        		
    			// Escribimos una cabecera (como si fuera un csv).
        		bw.write("id_proveedor;nombre;fecha_alta\n");
        		
        		// Escribimos la información de los proveedores del cliente dado.
        		// Al ser el mismo cliente no he visto necesario escribir el id del cliente.
        		for (Proveedor proveedor : proveedores) {
        			String text = String.
        					format("%d;%s;%s\n", 
        							proveedor.getId(),
        							proveedor.getNombre(),
        							proveedor.getFechaAltaStr());
        			bw.write(text);
        		}
        		
        		log.info("Datos escritos correctamente, procediendo a cerrar el buffer de escritura");
        		
        		bw.close();
        		log.info("Buffer cerrado correctamente");
        	}

		} catch (SQLException sqle) {
			log.error("No se ha podido conectar a la base de datos");
			System.exit(-1);
			
		} catch (IOException ioe) {
			log.error("No se ha podido crear o abrir el fichero");
			System.exit(-1);
		} finally {
			if (conn != null) {
				try {
					conn.close();
					log.info("Conexión cerrada con éxito");
				} catch (SQLException e) {
					log.error("No se ha podido cerrar la conexión");
				}	
			}
		}
        
        System.exit(0);
    }
    
    public static String myFunction(String input) {
    	return input + " with myFunction";
    }
}
