package com.java.maven.metrica_task.domain;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Proveedor {

	public Integer id;
	public String nombre;
	public Date fechaAlta;
	public Integer idCliente;
	
	public DateFormat dateFormat;
	
	public Proveedor() {
	}

	public Proveedor(Integer id, String nombre, Date fechaAlta, Integer idCliente) {
		this.id = id;
		this.nombre = nombre;
		this.fechaAlta = fechaAlta;
		this.idCliente = idCliente;
		
		this.dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}
	
	public String getFechaAltaStr() {
		return dateFormat.format(fechaAlta);
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	@Override
	public String toString() {
		return "Proveedor [id=" + id + ", nombre=" + nombre + ", fechaAlta=" + dateFormat.format(fechaAlta) + ", idCliente=" + idCliente
				+ "]";
	}
	
	
}
