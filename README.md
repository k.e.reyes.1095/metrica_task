# Tarea de Metrica para IMB #
---

### Stack tecnologico ##

Para desarrollar este pequeño ejercicio he utilizado:
- Maven 3.8.6
- Java 11
- MySQL 8.0

Las dependencias que he necesitado han sido:
|artifactId|version|
|----------|-------|
|mysql-connector-java|v8.0.29|
|slf4j-api|v1.7.36|
|slf4j-simple|v1.7.36|


### Instrucciones ###
Para trabajar con mySQL he creado una base de datos llamada **metrica_ibm**. En el fichero **script.sql** estan los comandos necesarios
para crear y rellenar la tabla **proveedores**. El usuario por defecto es *root* sin contraseña.

Para compilar el projecto maven debemos lanzar el siguiente comando (no es necesario):
~~~
mvn clean compile assembly:single
~~~

Para ejecutar el programa debemos buscar en /target el archivo jar llamado metrica_task-0.0.1-SNAPSHOT.jar
En una terminar escribimos:
~~~
java -jar metrica_task-1.0.0-jar-with-dependencies.jar "id"
~~~
Donde id es el identificador del cliente.

Esto genera en la carpeta output un fichero con el siguiente formato:
~~~
proveedores_cliente_"id".txt
~~~

Con el siguiente comando podemos ver el manifest del jar:
~~~
unzip -p metrica_task-1.0.0-jar-with-dependencies.jar META-INF/MANIFEST.MF
~~~